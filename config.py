USERNAME = 'root'
PASSWORD = ''
SERVER ='localhost'
DB = 'flask_gastos_pessoais'

SQLALCHEMY_DATABASE_URI = f'mysql://{USERNAME}:{PASSWORD}@{SERVER}/{DB}'
SQLALCHEMY_TRACK_MODIFICATIONS = True

SECRET_KEY = "minha-chave"
DEBUG=True