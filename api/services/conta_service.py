from api import db
from ..models import conta_model

def listar_contas():
    contas = conta_model.Conta.query.all()
    return contas

def cadastrar_conta(conta):
    conta_bd = conta_model.Conta(nome = conta.nome,descricao=conta.descricao,saldo=conta.saldo)
    db.session.add(conta_bd)
    db.session.commit()
    return conta_bd

def listar_conta_id(id):
    conta = conta_model.Conta.query.filter_by(id=id).first()
    return conta

def deletar_conta(conta):
    db.session.delete(conta)
    db.session.commit()

def editar_conta(conta_bd,conta_nova):
    conta_bd.nome = conta_nova.nome
    conta_bd.descricao = conta_nova.descricao
    conta_bd.saldo = conta_nova.saldo
    db.session.commit()
    return conta_bd