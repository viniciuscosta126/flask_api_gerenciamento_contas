from flask_restful import Resource
from ..services import conta_service
from flask import make_response, jsonify, request
from ..schemas import conta_schema
from ..entidades import conta
from api import api


class ContaList(Resource):
    def get(self):
        contas = conta_service.listar_contas()
        cs = conta_schema.ContaSchema(many=True)
        return make_response(cs.jsonify(contas), 200)

    def post(self):
        cs = conta_schema.ContaSchema()
        validate = cs.validate(request.json)
        if validate:
            return make_response(jsonify(validate), 400)
        else:
            nome = request.json['nome']
            descricao = request.json['descricao']
            saldo = request.json['saldo']
            conta_nova = conta.Conta(nome=nome,descricao=descricao,saldo=saldo)
            result = conta_service.cadastrar_conta(conta_nova)
            return make_response(cs.jsonify(result),201)

class ContaDetail(Resource):
    def get(self,id):
        cs = conta_schema.ContaSchema()
        conta_bd = conta_service.listar_conta_id(id)
        if conta_bd is None:
            return make_response(jsonify("Conta nao encontrada"),404)
        return make_response(cs.jsonify(conta_bd),200)

    def delete(self,id):
        cs = conta_schema.ContaSchema()
        conta = conta_service.listar_conta_id(id)
        if conta is None:
            return make_response("Conta nao encontrada",400)
        else:
            conta_service.deletar_conta(conta)
            return make_response(jsonify(''),204)

    def put(self,id):
        cs = conta_schema.ContaSchema()
        conta_bd = conta_service.listar_conta_id(id)
        if conta_bd is None:
            return make_response(jsonify("Conta nao encontrada"), 404)
        nome = request.json['nome']
        descricao = request.json['descricao']
        saldo = request.json['saldo']
        conta_nova = conta.Conta(nome=nome,descricao=descricao,saldo=saldo)
        conta_service.editar_conta(conta_bd,conta_nova)
        conta_atualizada = conta_service.listar_conta_id(id)
        return make_response(cs.jsonify(conta_atualizada),200)

api.add_resource(ContaList, '/contas')
api.add_resource(ContaDetail,"/contas/<int:id>")


