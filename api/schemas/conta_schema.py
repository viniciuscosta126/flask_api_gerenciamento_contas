from api import ma
from ..models import conta_model
from marshmallow import fields
class ContaSchema(ma.Schema):
    class Meta:
        model = conta_model.Conta
        load_instance = True
        fields = ['id','nome','descricao','saldo']

    nome = fields.String(required=True)
    descricao = fields.String(required=True)
    saldo = fields.Float(required=True)